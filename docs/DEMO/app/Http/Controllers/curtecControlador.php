<?php

namespace App\Http\Controllers;

use Request;
use App\carreraModel;
use App\categoriaModel;
use App\CursoModel;
use App\subTemaModel;
use App\temaModel;
use App\usuarioModel;

class curtecControlador extends Controller
{
    public function inicio(){return view('index');}
    public function info(){return view('info');}
    public function contacto(){return view('contacto');}
    public function Login(){return view('Login');}
    public function Reg(){return view('Reg');}
    
    public function insertAlumnos(){
    	
        $user = new usuarioModel;

        $u = usuarioModel::where('noControl','LIKE','%'. Request::get('input-noControl').'%')->first();
        if($u == null){
           $user->nombre = Request::get('input-nombre'); 
           $user->Apellido = Request::get('input-apellido');
           $user->Correo = Request::get('input-email');
           $user->noControl = Request::get('input-noControl');
           $user->password = Request::get('input-contraseña');
           $user->save();
           return view ('regExitoso');   
        }else{
            return view ('regFallido')
           ->with('n', $u);
        }

    }

    public function logAlumnos(){

        $user = new usuarioModel;
        
        $user = usuarioModel::where('noControl','LIKE','%'. Request::get('input-noControl').'%')->first();
        if($user ->password == Request::get('input-contraseña')){
          return view ('indexUsuario');
        }else{
          return view ('LogUsuario');
        }
    }

    public function indexUsuario(){
      return view ('indexUsuario');
    }

    public function infoUsuario(){
      return view ('infoUsuario');
    }

    public function contactoUsuario(){
      return view ('contactoUsuario');
    }

    public function subirUsuario(){
      return view ('subirUsuario');
    }
}


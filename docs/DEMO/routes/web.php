<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'curtecControlador@inicio');
Route::get('indexUsuario', 'curtecControlador@indexUsuario');

Route::get('info', 'curtecControlador@info');
Route::get('infoUsuario', 'curtecControlador@infoUsuario');


Route::get('contacto', 'curtecControlador@contacto');
Route::get('contactoUsuario', 'curtecControlador@contactoUsuario');

Route::get('Login', 'curtecControlador@Login');

Route::get('Reg', 'curtecControlador@Reg');

Route::post('callReg','curtecControlador@insertAlumnos');

Route::post('callLog','curtecControlador@logAlumnos');

Route::get('regExitoso','curtecControlador@regExitoso');

Route::get('regFallido','curtecControlador@regFallido');

Route::get('LogUsuario', 'curtecControlador@LogUsuario');

Route::get('subirUsuario', 'curtecControlador@subirUsuario');




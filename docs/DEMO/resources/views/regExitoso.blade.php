@extends('cabecera')
@section('contenido')		
<div id="espacioblanco">
	<div class="container principal">
		<div class="row">
			<div class="jumbotron col-md-6 col-md-offset-3">
				<i class="fa fa-check-circle fa-5x" aria-hidden="true"></i>
				<form action="Procesar.php" method="post" name="frm">
					
					<h2>Registro Exitoso!</h2>
					<a class="btn btn-primary" href="{{'Login'}}" role="button">Ingresar</a>					
				</form>
			</div>
		</div>
	</div>
</div>
@stop

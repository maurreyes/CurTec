@extends('cabeceraUsuario')
@section('contenidoUsuario')
<div id="espacioblanco">
	<div class="container principal">
		<center>
			<div class="jumbotron">
				<h1>¡Bienvenido a CurTec!</h1>
				<h2>Aprendizaje interactivo</h2>
				<i class="fa fa-book fa-5x" aria-hidden="true"></i>
			</div>
		</center>
	</div>
</div>

<div id="central">
	<div class="container principal">
		<center>
			<div class="jumbotron">
				<h2>Nuestro objetivo es proporcionar las herramientas por medio de las tecnologías 
					para el aprendizaje de futuras generaciones.</h2>
				<h3>¿Quiéres más información sobre nosotros? Da clic</h3><br>
				<a role="button" href="{{'infoUsuario'}}" class="btn btn-primary btn-lg">Aquí</a>
			</div>
		</center>
	</div>
</div>
<div id="final">
	<br>
	<center><p>
		Realizado por Alumnos de TecMM Zapopan para TecMM<br><br>
		<a href="#"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
		<a href="#"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
		<a href="#"><i class="fa fa-gitlab fa-2x" aria-hidden="true"></i></a><br><br>
	</p></center>
</div>
@stop
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="img/demo/icon.ico">
		<title>CurTec</title>
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/Style.css">
		<link rel="stylesheet" href="css/Styleindex.css">
		<script src="js/jquery-3.2.1.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<!--<script src="js/agregado.js"></script>
		<script src="js/main.js"></script>-->
	</head>

	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" 
					data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><img id="logo" src="img/demo/logo-mini.png" id="logo"></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-left">
						<li><a href="{{'indexUsuario'}}">Inicio</a></li>
						<li><a href="{{'infoUsuario'}}">Información</a></li>
						<li><a href="{{'contactoUsuario'}}">Contacto</a></li>
						<li class="dropdown">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cursos <span class="caret"></span></a>
				          <ul class="dropdown-menu">
				            <li><a href="#">Ing. Sistemas Computacionales</a></li>
				            <li><a href="#">Ing. Industrial</a></li>
				            <li><a href="#">Ing. Civil</a></li>
				            <li><a href="#">Ing. Electronica</a></li>
				            <li><a href="#">Ing. Electromecanica</a></li>
				            <li><a href="#">Ing. Gestion Empresarial</a></li>
				            <li><a href="#">Lic. Gastronomia</a></li>
				            <li><a href="#">Mtr. en Sistemas Computacionales</a></li>
				            
				          </ul>
				        </li>
				        <li><a href="{{'subirUsuario'}}">Subir Curso</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="{{'/'}}">Cerrar Sesion</a></li>
					</ul>
				</div>
			</div>
		</nav>
@yield('contenidoUsuario')

</body>
</html>

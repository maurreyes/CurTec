@extends('cabeceraUsuario')
@section('contenidoUsuario')
<div id="espacioblanco">
	<div class="container principal">
		<div class="row">
			<div class="jumbotron col-md-6 col-md-offset-3">
			<h2>Subir Archivo</h2><br>
				<form action="Procesar.php" method="post" name="frm">
				<label for="form-carrera">Categoria</label>
					<select class="form-control" id="form-carrera" required>
					  <option value="ingsis">Ing. Sistemas Computacionales</option>
					  <option value="ingind">Ing. Industrial</option>
					  <option value="ingciv">Ing. Civil</option>
					  <option value="ingelec">Ing. Electronica</option>
					  <option value="ingelm">Ing. Electromecanica</option>
					  <option value="ingges">Ing. Gestion Empresarial</option>
					  <option value="licgas">Lic. Gastronomia</option>
					  <option value="mtrsis">Mtr. en Sistemas Computacionales</option>
					</select><br>

					<div class= "form-group">
						<label for="input-nombre">Tema</label>
						<input type="text" name="input-tema" class="form-control" placeholder="Curso de POO" required>
					</div>

					 <div class="form-group">
					    <label for="exampleInputFile">SubTema</label>
					    <input type="text" name="input-subtema" class="form-control" placeholder="Nombre del subtema" required>
					    <br>
					    <input type="file" accept="video/mp4" max-size="2048" id="exampleInputFile" required>
					    <p class="help-block">Solo MP4</p>
					  </div>
					<input class="btn btn-primary" type="submit" value="Subir"/>
				</form>
			</div>
		</div>
	</div>
</div>
@stop